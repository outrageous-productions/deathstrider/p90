# P90

A weapon addon for [Deathstrider](https://gitlab.com/accensi/deathstrider).

---

An SMG loaded with .50AE, because why not.

PillowBlaster: Made to make one friend happy.

---

See [credits.txt](./credits.txt) for attributions.
